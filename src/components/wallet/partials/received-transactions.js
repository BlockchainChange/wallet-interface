import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Card, CardHeader } from 'reactstrap';
import TransactionPagination from './transaction-pagination';

  
export default withRouter(connect()((props) => {
        return (<Card className="mx-0 my-0">
                    <CardHeader className="color7-bg" id="ReceivedTransactionHeader">
                        <h3 className="mb-0 color4" data-toggle="collapse" data-target="#collapseReceivedTransaction" aria-expanded="true" aria-controls="collapseReceivedTransaction">
                            <strong className="display3"> Received </strong>
                            <span className="float-right"> Total: {props.transactions.length ? props.transactions.length : '0'} </span>
                        </h3>
                    </CardHeader>
                    <div id="collapseReceivedTransaction" className="collapse hide" aria-labelledby="ReceivedTransactionHeader">
                        { props.transactions.length  ? 
                             <TransactionPagination transactions={props.transactions} page={0} limit={15} received={true}/>
                        : ''}
                    </div>
                </Card>);
}));
