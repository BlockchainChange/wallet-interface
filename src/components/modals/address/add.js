import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
export default connect()((props) => {
        return (
                <div className="card">
                    <div className="card-header color7-bg" id="heading-right1">
                        <h3 className="mb-0 color4" data-toggle="collapse" data-target="#collapse-right1" aria-expanded="true" aria-controls="collapse-right1">
                            <strong className="display3">Awarded</strong> <span className="float-right">Total: {props.totalBlocks}  <small>BLOCKS</small></span>
                        </h3>
                    </div>
                    <div id="collapse-right1" className="collapse hide" aria-labelledby="heading1" data-parent="#accordion_sub-right1" >
                        <div className="card-body mx-0 my-0 px-0 py-0">
                        {props.blocks ? props.blocks.map((block) => {
                                return (<div className="card" key={`BlockId-${block.blockId}`}>
                                    <div className="card-header color8-bg" id="heading-level-right1">
                                        <div className="row color4" data-toggle="collapse" data-target="#collapse-level-right1" aria-expanded="true" aria-controls="collapse-level1">
                                            <div className="col"><small>BLOCK ID {block.blockId}</small></div>
                                            <div className="col"><small>AWARD {block.awardTotal}</small></div>
                                            <div className="col"><small>TRX COUNT {block.transactions.length}</small></div>
                                            <div className="col"><small>{ block.transactions.length ? 'SumTheAmounts' : 0 } {"\u26B7"} TOTAL</small></div>
                                            <div className="col"><small>{ block.transactions.length ? 'SumTheFees' : 0 } {"\u26B7"} FEES </small> <span className="float-right"><Link to={`/blocks/${block.blockId}`} ><i className="fa fa-search"></i></Link></span></div>
                                        </div>
                                    </div>
                                </div>)}) : ''} 

                        </div>
                    </div>
                </div>);
})