import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { login } from '../../actions';
import { connect } from 'react-redux';
import Header from '../header';
import Footer from '../footer';
import {
  UncontrolledAlert,
  Container,
  Row,
  Col,
  Badge,
  Button,
  Input
} from 'reactstrap';
class SignIn extends Component {  
  handleUploadFile(event){
    const reader = new FileReader();
    reader.readAsBinaryString(event.target.files[0]);
    reader.onload = () => {        
        const privateKey = reader.result;
        this.props.login(privateKey, this.props.history);
    };
  }
  
  renderAlert() {
    if (!this.props.error) return null;
    return (
        <UncontrolledAlert color="danger">
            <h3>{this.props.error}</h3>
        </UncontrolledAlert>   
        );
  }
  
  loadKeyFromLocalStorage() {
    this.props.login(localStorage.getItem('privateKey'), this.props.history);
  }
  
  render() {
    return (
      [<Header key="header"/>,
      <section key="body">
        <Container fluid>
          <Container>
            <Row>
              <Col className="text-center">
                <h1><span className="text-primary">Sign In</span> to the <span style={{textShadow: '2px 2px 4px #007bff'}}>Wallet Interface</span></h1>
                <UncontrolledAlert color="warning"><strong>Note :</strong> Don&#39;t lose your wallet key! You want to store your private key in a safe place for accessing your wallet.</UncontrolledAlert>
              </Col>
            </Row>
            {this.renderAlert()}
            <Row className="justify-content-center text-center">
              <Col className="col align-self-center">
                <Button outline color="primary" size="lg" className="w-100" onClick={this.loadKeyFromLocalStorage.bind(this)}>Browser Login</Button>
              </Col>
              <Badge className="badge-secondary align-self-center">OR</Badge>
              <Col>
                <Input color="primary" bsSize="lg" className="w-100" onChange={this.handleUploadFile.bind(this)} name="privateKeyFile" type="file"/>
              </Col>
            </Row>
          </Container>
        </Container>
      </section>,
      <Footer key="footer"/>]);
  }
}

const mapStateToProps = state => {
  return {
    error: state.auth.error,
    authenticated: state.auth.authenticated
  };
};

SignIn = connect(mapStateToProps, { login })(SignIn);

export default reduxForm({
  form: 'signin',
  fields: ['privateKey']
})(SignIn);
