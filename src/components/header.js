import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import fileDownload from "js-file-download";
import HOST_URL from "../configs";

import { signOut, getAlerts, toggleAlert, getAddressBook } from "../actions";

import {
  Container,
  ButtonGroup,
  ButtonToolbar,
  Button,
  Row,
  Col,
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  UncontrolledDropdown,
  Alert,
  ListGroup,
  ListGroupItem as ListItem,
} from "reactstrap";

import twitter_logo from "../assets/images/twitter.svg";
import instagram_logo from "../assets/images/instagram.svg";
import discord_logo from "../assets/images/discord.svg";
import youtube_logo from "../assets/images/youtube.svg";
import bitbucket_logo from "../assets/images/bitbucket.svg";

const stylePointer = { cursor: "pointer" };
const lightShadow = { textShadow: "none" };
const darkCombo = {};

Object.assign(darkCombo, lightShadow, stylePointer);

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
    this.toggle = this.toggle.bind(this);
    this.getAlerts = this.props.getAlerts.bind(this);
    this.getAddressBook = this.props.getAddressBook.bind(this);
    /*
    this.props.dangerAlert({
      alert_type: 'danger',
      visible: true,
      _html: <span><strong>Warning : </strong> We you browser storage and cookies, by using this site you agree to allowing us to use this data.</span>
      }
      ,this.props.alerts);
    */
  }

  componentWillMount() {
    this.getAlerts();
    if (this.props.authenticated) this.getAddressBook();
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  signOut() {
    this.props.signOut();
    this.props.history.push("/");
  }

  saveToLocalStorage() {
    localStorage.setItem("privateKey", this.props.auth.wallet.walletPrivateKey);
  }

  getLinks() {
    if (this.props.authenticated) {
      return [
        <NavItem key={1}>
          <NavLink tag={Link} to="/blocks" style={lightShadow}>
            Explorer
          </NavLink>
        </NavItem>,
        <NavItem key={2}>
          <Link
            component={Link}
            tag={Link}
            to="/user/wallet"
            style={lightShadow}
          >
            Wallet&nbsp;Overview
          </Link>
        </NavItem>,
        <NavItem key={3}>
          <Link to="/user/wallet/send-transaction" style={lightShadow}>
            Send
          </Link>
        </NavItem>,
        <UncontrolledDropdown key={4} className="nav-item" style={stylePointer}>
          <DropdownToggle tag="nav-item">
            <NavLink style={lightShadow}>Address Book </NavLink>
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem
              data-toggle="modal"
              data-target="#addressBookModal"
              style={stylePointer}
            >
              View
            </DropdownItem>
            <DropdownItem
              onClick={this.downloadAddressBook.bind(this)}
              style={stylePointer}
            >
              Download
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>,
      ];
    }
    return [
      <NavItem key={1}>
        <NavLink tag={Link} to="/">
          <strong>Explorer</strong>
        </NavLink>
      </NavItem>,
      <NavItem key={2}>
        <NavLink tag={Link} to="/blocks/0">
          <strong>Genesis</strong>
        </NavLink>
      </NavItem>,
      <NavItem key={3}>
        <NavLink tag={Link} to="/node-list">
          <strong>Node List</strong>
        </NavLink>
      </NavItem>,
    ];
  }

  onDismiss(e) {
    let alertId = e.target.parentElement.dataset.alertid;
    if (alertId === undefined)
      alertId = e.target.parentElement.parentElement.dataset.alertid;
    if (alertId !== undefined) {
      const alerts = this.props.alerts.slice();
      alerts[alertId].visible = !alerts[alertId].visible;
      this.props.toggleAlert(alerts);
    }
  }

  clearLocalStorage() {
    localStorage.setItem("privateKey", null);
  }

  downloadFile() {
    fileDownload(
      this.props.auth.wallet.walletPrivateKey,
      "PrivateWalletKey.pem"
    );
  }

  downloadAddressBook() {
    fileDownload(JSON.stringify(this.props.addressBook), "AddressBook.json");
  }

  render() {
    return (
      <div>
        <section>
          <Container fluid>
            <Row className="justify-content-center no-gutters">
              <Col>
                <ButtonGroup>
                  <ButtonToolbar size="sm">
                    <h4 className="text-primary mb-0" style={darkCombo}>
                      <sup>
                        <small>
                          Blockchain <br />
                          Change
                        </small>
                      </sup>
                    </h4>
                    <a
                      className="btn btn-sm my-auto border-0"
                      target="_default"
                      href="//twitter.com/BCCInit"
                    >
                      <img
                        src={twitter_logo}
                        width="32px"
                        alt="Follow us on Twitter"
                      />
                    </a>
                    <a
                      className="btn btn-sm my-auto border-0"
                      target="_default"
                      href="//www.instagram.com/dreamingrainbow/"
                    >
                      <img
                        src={instagram_logo}
                        width="32px"
                        alt="Check out our Instagram"
                      />
                    </a>
                    <a
                      className="btn btn-sm my-auto border-0"
                      target="_default"
                      href="//discord.gg/vFeZuYj"
                    >
                      <img
                        src={discord_logo}
                        width="32px"
                        alt="You're invited to our Discord community, where we discuss blochchain change!"
                      />
                    </a>
                    <a
                      className="btn btn-sm my-auto border-0"
                      target="_default"
                      href="//www.youtube.com/channel/UCGeS7FCwQznSFAiITKryRKg"
                    >
                      <img
                        src={youtube_logo}
                        width="32px"
                        alt="Subscribe to us on Youtube for the lastest new's and updates."
                      />
                    </a>
                    <a
                      className="btn btn-sm my-auto border-0"
                      target="_default"
                      href="//bitbucket.org/BlockchainChange"
                    >
                      <img
                        src={bitbucket_logo}
                        width="32px"
                        alt="Our BitBucket to view our public repositories"
                      />
                    </a>
                  </ButtonToolbar>
                </ButtonGroup>
              </Col>
              <Col className="text-center align-self-center">
                <ListGroup>
                  <ListItem className="py-0 pl-0 pr-0 border-0">
                    <Button disabled outline className="border-0">
                      Current Node {`${HOST_URL}`}
                    </Button>
                  </ListItem>
                </ListGroup>
              </Col>
              {this.props.authenticated ? (
                <Col className="text-right align-self-center">
                  <Link
                    to="#"
                    style={darkCombo}
                    onClick={this.signOut.bind(this)}
                  >
                    Sign-Out
                  </Link>{" "}
                  |{" "}
                  <Link
                    to="#"
                    style={darkCombo}
                    onClick={this.clearLocalStorage.bind(this)}
                  >
                    Clear Browser Key
                  </Link>{" "}
                  |{" "}
                  <Link
                    to="#"
                    onClick={this.saveToLocalStorage.bind(this)}
                    style={darkCombo}
                  >
                    Save Key To Browser
                  </Link>{" "}
                  |{" "}
                  <Link
                    to="#"
                    style={darkCombo}
                    onClick={this.downloadFile.bind(this)}
                  >
                    Download Key
                  </Link>
                </Col>
              ) : (
                <Col className="text-right align-self-center">
                  <Link to="/user/sign-in" style={darkCombo}>
                    Sign-In
                  </Link>{" "}
                  |{" "}
                  <Link to="/user/wallet/create" style={darkCombo}>
                    Create A Wallet
                  </Link>
                </Col>
              )}
            </Row>
          </Container>
        </section>
        <Navbar color="info" expand="md" dark className="navbar-header">
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="mx-auto" navbar>
              {this.getLinks()}
            </Nav>
          </Collapse>
        </Navbar>
        {this.props.alerts.length ? (
          <Container fluid className="mt-2 px-0">
            <Container>
              <Row>
                <Col>
                  {this.props.alerts.map((alert, i) => {
                    return (
                      <Alert
                        color={alert.alert_type}
                        isOpen={alert.visible}
                        toggle={this.onDismiss.bind(this)}
                        data-alertid={i}
                        key={i}
                      >
                        {alert._html}
                      </Alert>
                    );
                  })}
                </Col>
              </Row>
            </Container>
          </Container>
        ) : (
          ""
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    addressBook: state.addressBook,
    alerts: state.alerts,
    auth: state.auth,
    authenticated: state.auth.authenticated,
  };
};

export default withRouter(
  connect(mapStateToProps, {
    signOut,
    getAlerts,
    toggleAlert,
    getAddressBook,
  })(Header)
);
