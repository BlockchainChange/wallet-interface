import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Row, Col, Button } from 'reactstrap';
import { addAddress, removeAddress, deleteAddressBook } from '../../../actions';
import fileDownload from 'js-file-download';
const stylePointer = { cursor:"pointer"};

class AddressBook extends Component {
  
  downloadAddressBook() {
    fileDownload(JSON.stringify(this.props.addressBook), 'AddressBook.json');    
  }
  
  addAddress( ) { 
    const addressNick = this.refs.nickname.value;
    let receiver = this.refs.receiver.value;
    this.refs.nickname.value = '';
    this.refs.receiver.value = '';
    /* TODO :: validate address, and set to object */
    this.props.addAddress(addressNick, receiver);
    //this.setState({addressBook:this.props.addressBook});
  }
  
  deleteAddressBook(){
    const r = window.confirm("Are you sure you want to delete your Address Book?");
    if( r === true ) {
      this.props.deleteAddressBook();
    }
  }
  
  removeAddress(e){
    const r = window.confirm("Are you sure you want to delete this address?");
    if( r === true ) {
      this.props.removeAddress(e.target.dataset.nickname);
    }
  }
  
  render(){
        return(
        <div className="modal fade" id="addressBookModal" tabIndex="-1" role="dialog" aria-labelledby="AddressBookModalLabel" aria-hidden="true" style={{zIndex:1090}}>
          <div className="mt-5 pt-5 mb-5 pb-5">
            <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <input ref="nickname" name="nickname" type="text" className="form-control" placeholder="Nickname" />
                  <div className="input-group col-6 pl-auto">
                    <input ref="receiver" name="receiver" type="text" className="form-control" placeholder="Address" />
                    <div className="input-group-append">
                        <Button size="sm" onClick={this.addAddress.bind(this)}>Add</Button>
                    </div>
                  </div>
                  <Button onClick={this.downloadAddressBook.bind(this)}>
                    <span aria-hidden="true" ><i className="fa fa-download"></i></span>
                  </Button>
                  <Button onClick={this.deleteAddressBook.bind(this)}>
                    <span aria-hidden="true"><i className="fa fa-trash"></i></span>
                  </Button>
                  <Button className="close " data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </Button>
                </div>
                <div className="modal-body color18">
                {this.props.addressBook.length !== 0 ? this.props.addressBook.map((address, i) => {
                    return (<Row className="justify-content-center" key={address.nickname + i}>
                        <Col size={1} md={1} className="align-self-center"><img alt="QR Address Tag" className="img-thumbnail" src="https://placehold.it/34x34"/></Col>
                        <Col size={1} md={1} className="align-self-center">{address.nickname}</Col>
                        <Col style={stylePointer} title="Click to Copy to Clipboard" onClick={(e)=>{
                                          var textField = document.createElement('textarea');
                                          textField.innerText = e.target.innerText;
                                          document.body.appendChild(textField);
                                          textField.select();
                                          document.execCommand('copy');
                                          textField.remove();                                          
                                          }} className="align-self-center">{address.receiver}</Col>
                        <Col size={1} md={1} className="align-self-center">
                            <Button size="sm" title="Click to remove." onClick={this.removeAddress.bind(this)} data-nickname={ address.nickname } >&times;</Button>
                        </Col>
                    </Row>);                  
                }) : ''}
                </div>
              </div>
            </div>
          </div>
        </div>        
  )}
};

const mapStateToProps = state => {
  return {
    addressBook : state.addressBook
  };
};

export default withRouter(connect(mapStateToProps, { addAddress, removeAddress, deleteAddressBook})(AddressBook));
