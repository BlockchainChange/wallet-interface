import {
    MISSING_PARAMETER,
    INVALID_BALANCE,
    INVALID_KEY,
    TRANSACTION_REJECTED,
    TRANSACTION_RECORDED,
    TRANSACTION_HASHED,
    TRANSACTION_FINAL_HASH,
    TRANSACTION_LOOKUP,
    LOOKUP_ERROR
  } from '../actions';
  
  export default (transaction = {recorded:false}, action) => {
    switch (action.type) {
      case MISSING_PARAMETER:
        return { ...transaction, recorded: false, error: action.payload };
      case INVALID_BALANCE:
        return { ...transaction, recorded: false, error: action.payload };
      case INVALID_KEY:
        return { ...transaction, recorded: false, error: action.payload };
      case TRANSACTION_REJECTED:
        return { ...transaction, recorded: false, error: action.payload };
      case TRANSACTION_RECORDED:
        return { ...transaction, recorded: true, recordedTransaction: action.payload};
      case TRANSACTION_FINAL_HASH:
        return { ...transaction, recorded: false, hash: action.payload};
      case TRANSACTION_HASHED:
        return { ...transaction, recorded: false, hash: action.payload};
      case LOOKUP_ERROR:
        return { ...transaction, recorded: false, error: action.payload };
      case TRANSACTION_LOOKUP:
        return {...action.payload, recorded: false };
      default:
        return transaction;
    }
  };