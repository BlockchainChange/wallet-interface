import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import axios from "axios";
import Header from "../header";
import Footer from "../footer";
import HOST_URL from "../../configs";
import { Container, Row, Col, Card } from "reactstrap";
class NBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      block: this.props.match.params.blockId,
    };
  }
  componentWillMount() {
    axios
      .get(`${HOST_URL}/blocks/${this.props.match.params.blockId}`)
      .then((response) => {
        console.log(response.data);
        this.setState({ loaded:true, block: response.data });
      })
      .catch((err) => {
        console.log(err);
        return 0;
      });
  }
  componentWillUpdate() {
    const id = this.props.history.location.pathname.split("/").pop();
    if (this.props.match.params.blockId !== id)
      axios
        .get(`${HOST_URL}${this.props.history.location.pathname}`)
        .then((response) => {
          this.setState({ loaded:true, block: response.data });
        })
        .catch((err) => {
          console.log(err);
          return 0;
        });
  }
  render() {
    return (
      <>
        <Header key="header" />
        {this.state.loaded ? (
          <section key={`body${this.state.block.blockId}`}>
            <Container fluid>
              <Container>
                <NBlockHeader />
                <Row>
                  <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                    <Card id="accordion">
                      <div
                        className="card-header color8-bg color5 text-center"
                        id={`header_${this.state.block.blockId}`}
                        data-toggle="collapse"
                        data-target={"#collapse_" + this.state.block.blockId}
                        aria-expanded={
                          this.state.block.blockId === 0 ? true : false
                        }
                        aria-controls={`collapse_${this.state.block.blockId}`}
                      >
                        <div className="row">
                          <div className="col-4 text-left">
                            <strong>Block </strong>
                            {this.state.block.blockId}
                          </div>
                          <div className="col-4">
                            <strong>Forged By </strong>
                            {this.state.block.response.forgedBy}
                          </div>
                          <div className="col-4 text-right">
                            <strong>Award </strong>
                            {this.state.block.response.awardTotal}
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-4 text-left">
                            <strong>Difficulty </strong>
                            {this.state.block.response.difficulty}
                          </div>
                          <div className="col-4">
                            <strong>Time-Complexity </strong>
                            {this.state.block.response.timeComplexity}{" "}
                          </div>
                          <div className="col-4 text-right">
                            <strong>Proof </strong>
                            {this.state.block.response.proof}
                          </div>
                        </div>
                        <div className="row">
                          <div className="col">
                            <h3 className="mb-0 dispaly-4 color5 text-center">
                              {new Date(
                                this.state.block.response.timestamp
                              ).toUTCString()}
                            </h3>
                          </div>
                        </div>
                      </div>
                      <div
                        id={"collapse_" + this.state.block.blockId}
                        className={
                          this.state.block.blockId === 0
                            ? "collapse show"
                            : "collapse"
                        }
                        aria-labelledby={`header_${this.state.block.blockId}`}
                        data-parent="#accordion"
                      >
                        <div className="card-body px-0 mx-1">
                          <div className="row mx-2">
                            <div className="col-md-6">
                              <p>
                                <strong>Previous Hash : </strong>
                                <br />
                                {this.state.block.response.blockId - 1 !==
                                -1 ? (
                                  <Link
                                    to={`/blocks/${
                                      this.state.block.response.blockId - 1
                                    }`}
                                  >
                                    <code>
                                      {this.state.block.response.previousHash.substr(
                                        7
                                      )}
                                    </code>
                                  </Link>
                                ) : (
                                  "0"
                                )}
                              </p>
                              <p>
                                <strong>Hash : </strong>
                                <br />
                                <code>
                                  {this.state.block.response.hash.substr(7)}
                                </code>
                              </p>
                            </div>
                            <div className="col-md-6">
                              <strong>Awarded To: </strong>
                              <div className="card align-self-center color18-bg color4">
                                <div className="card-body text-center px-0 align-self-center">
                                  <pre className="color3 ">
                                    {this.state.block.response.awardWallet.a}
                                    <br />
                                    {this.state.block.response.awardWallet.b}
                                  </pre>
                                </div>
                              </div>
                            </div>
                          </div>
                          {this.state.block.response.transactions !==
                          undefined ? (
                            <div id="accordion_2">
                              {this.state.block.response.transactions
                                .reverse()
                                .map((transaction, i) => {
                                  return (
                                    <div
                                      className="card px-0 mx-1 py-0"
                                      key={
                                        "trx" + transaction.transactionsIndex
                                      }
                                    >
                                      <div
                                        className="card-header color8-bg"
                                        id={
                                          "heading" +
                                          transaction.transactionsIndex
                                        }
                                      >
                                        <h3
                                          className="mb-0 color4"
                                          data-toggle="collapse"
                                          data-target={
                                            "#collapse" +
                                            transaction.transactionsIndex
                                          }
                                          aria-expanded={i === 0 ? true : false}
                                          aria-controls={
                                            "collapse" +
                                            transaction.transactionsIndex
                                          }
                                        >
                                          <div className="row">
                                            <div className="col">
                                              <strong>
                                                Transactions Index{" "}
                                              </strong>
                                              <Link
                                                to={`/transactions/${transaction.transactionsIndex}`}
                                              >
                                                {transaction.transactionsIndex}
                                              </Link>
                                            </div>
                                            <div className="col">
                                              <strong>Amount </strong>
                                              {transaction.amount} coin
                                            </div>
                                          </div>
                                        </h3>
                                      </div>

                                      <div
                                        id={
                                          "collapse" +
                                          transaction.transactionsIndex
                                        }
                                        className={
                                          i === 0 ? "collapse show" : "collapse"
                                        }
                                        aria-labelledby={
                                          "heading" +
                                          transaction.transactionsIndex
                                        }
                                        data-parent="#accordion_2"
                                      >
                                        <div className="card-body px-0 mx-1">
                                          <div className="row">
                                            <div className="col-md-12 col-lg-6">
                                              <h3>
                                                <strong>Previous Hash</strong>
                                              </h3>{" "}
                                              {transaction.data.length ? (
                                                <Link
                                                  to={`/transactions/${
                                                    transaction.transactionsIndex -
                                                    1
                                                  }`}
                                                >
                                                  <code>
                                                    {transaction.txPreviousHash.substr(
                                                      7
                                                    )}
                                                  </code>
                                                </Link>
                                              ) : (
                                                "NONE"
                                              )}
                                            </div>
                                            <div className="col-md-12 col-lg-6">
                                              <h3>
                                                <strong>Hash </strong>
                                              </h3>{" "}
                                              <code>
                                                {transaction.txHash.substr(7)}
                                              </code>
                                            </div>
                                          </div>
                                          <div className="row">
                                            <div className="col-sm-12 col-md-6">
                                              <strong>Sender : </strong>
                                              <div className="card align-self-center color18-bg">
                                                <div className="card-body text-center align-self-center">
                                                  <pre className="color3">
                                                    {transaction.sender.a}
                                                    <br />
                                                    {transaction.sender.b}
                                                  </pre>
                                                </div>
                                              </div>
                                            </div>

                                            <div className="col-sm-12 col-md-6 ">
                                              <strong>Receiver : </strong>
                                              <div className="card align-self-center color18-bg">
                                                <div className="card-body text-center align-self-center">
                                                  <pre className="color3">
                                                    {transaction.receiver.a}
                                                    <br />
                                                    {transaction.receiver.b}
                                                  </pre>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="row pt-2">
                                            <div className="col">
                                              <ul
                                                className="nav nav-tabs"
                                                role="tablist"
                                              >
                                                {transaction.data[0].length !==
                                                0 ? (
                                                  <li className="nav-item">
                                                    <a
                                                      className="nav-link active"
                                                      href="#message"
                                                      role="tab"
                                                      data-toggle="tab"
                                                    >
                                                      Message
                                                    </a>
                                                  </li>
                                                ) : (
                                                  ""
                                                )}
                                                <li className="nav-item">
                                                  <a
                                                    className="nav-link"
                                                    href="#buzz"
                                                    role="tab"
                                                    data-toggle="tab"
                                                  >
                                                    Data-Segments
                                                  </a>
                                                </li>
                                              </ul>

                                              <div className="tab-content">
                                                <div
                                                  role="tabpanel"
                                                  className="tab-pane fade active show"
                                                  id="message"
                                                >
                                                  {transaction.data[0]
                                                    .length !== 0 ? (
                                                    <div className="card align-self-center color18-bg">
                                                      <div className="card-body px-0 align-self-center">
                                                        <pre className="mx-0 my-0">
                                                          {
                                                            transaction.data[0]
                                                              .message
                                                          }
                                                        </pre>
                                                      </div>
                                                    </div>
                                                  ) : (
                                                    ""
                                                  )}
                                                </div>
                                                <div
                                                  role="tabpanel"
                                                  className="tab-pane fade"
                                                  id="buzz"
                                                >
                                                  <div className="card align-self-center color16-bg">
                                                    <div className="card-body px-0 align-self-center">
                                                      Storing public transaction
                                                      on the blockchain.
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  );
                                })}
                            </div>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </Card>
                  </div>
                </Row>
              </Container>
            </Container>
          </section>
        ) : (
          ""
        )}
        <Footer key="footer" />
      </>
    );
  }
}
const mapStateToProps = (state) => {
  // add spent tansactions and unspent transactions
  return {
    auth: state.auth,
    error: state.auth.error,
  };
};
export default withRouter(connect(mapStateToProps, {})(NBlock));
function NBlockHeader() {
  return (
    <Row>
      <Col className="text-center">
        <h1 style={{ textShadow: "2px 2px 2px #007bff" }}>Block Explorer</h1>
      </Col>
    </Row>
  );
}
