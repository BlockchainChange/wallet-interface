import React, { Component } from 'react';
import Header from './header';
import Footer from './footer';
import {
	Container,
	Row, 
	Col,
	Button 
} from 'reactstrap';
import nodeList from '../configs/node-list.json';
nodeList[0].created = Date.now();

export default class Node_List extends Component {
    render() {
      	return (  
      	[<Header key="header"/>,
   		<section key="body">
			<Container fluid>
				<Container>
					<Row>
						<Col className="text-center">
							<h1>Node List <span className="pull-right"><Button size="sm" color="success" outline><i className="fa fa-plus"></i></Button></span></h1>
						</Col>
					</Row>
					<Row>
						<Col size={12} sm={3} md={3} lg={3}>
							<h3>Address</h3>				
						</Col>
						<Col size={12} sm={1} md={1} lg={1}>
							<h3>Port</h3>
						</Col>
						<Col size={12} sm={3} md={3} lg={3} >
							<h3>Down Count</h3>
						</Col>
						<Col size={12} sm={2} md={2} lg={2}>
							<h3>Status</h3>
						</Col>
						<Col size={12} sm={3} md={3} lg={3}>
							<h3>Last Update</h3>
						</Col>
					</Row>
				{nodeList.map((node, i) => 
					<Row key={`node-${i}`}>
						<Col size={12} sm={3} md={3} lg={3}>
							{node.address}				
						</Col>
						<Col size={12} sm={1} md={1} lg={1}>
							{node.port}
						</Col>
						<Col size={12} sm={3} md={3} lg={3} >
							{node.downCount}
						</Col>
						<Col size={12} sm={2} md={2} lg={2}>
							{node.status}
						</Col>
						<Col size={12} sm={3} md={3} lg={3}>
							{new Date(node.created).toUTCString()}
						</Col>
				</Row>)}		
				</Container>
			</Container>
		</section>,
		<Footer key="footer"/>]
    );
    }
  }
  
