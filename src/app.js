import React, { Component } from 'react';
import { withRouter, BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ChainOverview from './components/blockchain/overview';
import LastBlock from './components/blockchain/last-block';
import NBlock from './components/blockchain/n-block';
import CreateWallet from './components/wallet/create';
import RequireAuth from './components/auth/require-auth';
import SignIn from './components/auth/sign-in';
import WalletOverview from './components/wallet/overview';
import SendTransaction from './components/wallet/send';
import N_Transaction from './components/transactions/n-transaction';
import Node_List from './components/node-list';
import Not_Found from './components/not-found';
import 'jquery';
import 'bootstrap';

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import './components/footerHover.css';

//import './assets/css/style.css';

class App extends Component {
  render() {
    return (
      <Router >
        <Switch>
          <Route path="/" component={ChainOverview} exact />
          <Route path="/user/wallet/create" component={CreateWallet}  exact />
          <Route path="/node-list" component={Node_List} exact  />
          <Route path="/blocks" component={ChainOverview} exact/>
          <Route path="/blocks/last-block" component={LastBlock} exact/>
          <Route path="/blocks/:blockId" component={NBlock} exact  />
          <Route path="/transactions/to-process" component={ChainOverview}  exact />
          <Route path="/transactions/last-transaction" component={ChainOverview}  exact />
          <Route path="/transactions/last-fifteen-transactions" component={ChainOverview}  exact />
          <Route path="/transactions/:transactionIndex" component={N_Transaction} exact  />
          <Route path="/user/wallet" component={RequireAuth(WalletOverview)} exact />
          <Route path="/user/wallet/send-transaction" component={RequireAuth(SendTransaction)} exact />
          <Route path="/user/sign-in" component={SignIn} exact  />
          <Route component={Not_Found}  />
          
        </Switch>        
      </Router>
    );
  }
}

export default withRouter(App);
