import {
    BLOCKCHAIN_ERROR,
    BLOCKCHAIN_LAST_FIFTEEN,
    BLOCKCHAIN_BY_ID,
  } from '../actions';
  
  export default (blockchain = [], action) => {
    switch (action.type) {
      case BLOCKCHAIN_ERROR:
        return action.payload;
      case BLOCKCHAIN_LAST_FIFTEEN:
        return action.payload.data;
      case BLOCKCHAIN_BY_ID:
        return action.payload.data; 
      default:
        return blockchain;
    }
  };