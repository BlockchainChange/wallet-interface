import React, {Component} from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { Card, CardHeader, CardBody, Row, Col, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
const stylePointer = { cursor:"pointer"};

class TransactionPagination extends Component {
  constructor(props){
      super(props);  
      console.log(props);
      const {transactions, page, limit, received } = this.props;
      const pageCount = Math.ceil(transactions.length || 0 / limit);
      let pages = [];
      transactions.reverse();
      let transaction = transactions.pop(); 
      while(transactions.length){
        let _page = [];
        if(_page.length === 0 && transaction !== undefined) {
            _page.push(transaction);   
        }      
        while(_page.length <= limit && transaction !== undefined) {
            transaction = transactions.pop();
            if (transaction !== undefined)
              _page.unshift(transaction);
        }
        _page.reverse();
        pages.unshift(_page);
      }
      pages.reverse();
      this.state = {
        pages,
        page,
        limit,
        pageCount,
        received : received || false
      };    
  }
  
  changePage(e) {
      let pageToGoTo = e.target.dataset.page;
      if(pageToGoTo <= 0) { pageToGoTo = 0;}
      this.setState({
          pages : this.state.pages,
          page : pageToGoTo,
          limit : this.state.limit,
          pageCount : this.state.pageCount,
          received : this.state.received || false
      });
  }
  
  render(){
    
    return (
      <CardBody className="mx-0 my-0 px-0 py-0">
           {this.state && this.state.pages.length ?                        
                <Card key={`${this.state.received ? 'received-top-pagination-card' : 'sent-top-pagination-card'}`}>
                    <CardHeader className="color4-bg justify-content-center text-center">
                        <Pagination size="sm" className="justify-content-center mb-0">
                            <PaginationItem className={`${this.state.page <= 0 ? 'disabled' : ''}`}>
                              <PaginationLink previous onClick={this.changePage.bind(this)} data-page={this.state.page - 1 <= 0 ? 0 : this.state.page - 1 } />
                            </PaginationItem>
                            {this.state.pages.map((Page, i) => {
                              return (<PaginationItem key={i}>
                                <PaginationLink onClick={this.changePage.bind(this)} data-page={i} className="color15-bg color4">
                                  {i + 1}
                                </PaginationLink>
                              </PaginationItem>);})}                                        
                            <PaginationItem className={`${this.state.page >=  this.state.pages.length || this.state.pages.length <= 5 ? 'disabled' : ''}`}>
                              <PaginationLink next onClick={this.changePage.bind(this)} data-page={this.state.page+1} />
                            </PaginationItem>
                        </Pagination>
                        {`Page : ${Number(this.state.page) + 1 } of ${this.state.pageCount}`}
                    </CardHeader>    
                </Card>
            : ''}            
          {this.state && this.state.pages.length ? this.state.pages[this.state.page].map(card => {           
            return (<Card key={`${this.state.received ? 'received-card-' : 'sent-card-'}-${card.transactionsIndex}`}>
                <CardHeader>
                    <Row>
                        <Col md={4} sm={12}>
                            <small>INDEX {card.transactionsIndex}</small>
                            <span className="float-right"><small> {card.amount}</small></span>
                        </Col>
                        <Col md={2} sm={12}><small>{card.fee} FEE</small></Col>
                        <Col>
                          <small>
                            <span style={stylePointer} onClick={()=>{alert(card.data[0].message); return false;}}><i className="fa fa-comments"></i></span> &nbsp;
                            <span style={stylePointer} onClick={()=>{alert('Verify Transaction Hash'); return false;}}>Verify Hash <i className="fa fa-question"></i></span> &nbsp;
                            <span style={stylePointer} onClick={()=>{alert('Verify Transaction Signature'); return false;}}>Verify Signature <i className="fa fa-question"></i></span> &nbsp;
                            <span style={stylePointer} onClick={()=>{alert('Add receiver to address book'); return false;}} title="Add Address to address book"><i className="fa fa-user-plus"></i></span> &nbsp;
                            <Link to={`/transactions/${card.transactionsIndex}`}><i className="fa fa-search"></i></Link>
                          </small>
                        </Col>
                    </Row>
                </CardHeader>
            </Card>)}) : 'No Transactions Found'}
          
          </CardBody>);
    }
}


TransactionPagination = connect()(TransactionPagination);

export default withRouter(TransactionPagination);
