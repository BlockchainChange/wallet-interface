import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { Row, Col, Container, Navbar } from "reactstrap";
import TermsOfService from "./modals/terms-of-service";
import PrivacyPolicy from "./modals/privacy-policy";
import AddressBook from "./modals/address/book";

const stylePointer = { cursor: "pointer" };
const lightShadow = { textShadow: "none" };

class Footer extends Component {
  getCopyright() {
    const lightCombo = {};
    Object.assign(lightCombo, lightShadow, stylePointer);
    return (
      <footer className="bg-dark text-info" key="footer-copyright">
        <Container fluid>
          <Row>
            <Col className="text-center">
              <p>
                &copy; Copyright 2018 - {new Date().getFullYear()} Michael A. Dennis All Rights Reserved.
                <br />
                <button
                  className="btn btn-sm btn-link"
                  data-toggle="modal"
                  data-target="#termsOfService"
                  // style={lightCombo}
                >
                  Terms
                </button>{" "}
                |{" "}
                <button
                  className="btn btn-sm btn-link"
                  data-toggle="modal"
                  data-target="#privacyPolicyModal"
                  // style={lightCombo}
                >
                  Privacy Policy
                </button>
              </p>
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }

  render() {
    return [
      <Navbar
        color="info"
        expand="md"
        dark
        key="footer-body"
        className="navbar-footer"
      >
        <Row className="align-content-justified w-100 ">
          <Col className="self-align-center text-center" sm={12} md={4}>
            <h2 style={lightShadow}>
              <strong>Blockchain</strong>
            </h2>
            <ul className="list-unstyled">
              <li>
                <Row>
                  <Col className="align-self-center text-center h-100">
                    <Link to="/" className="">
                      Explorer
                    </Link>
                  </Col>
                </Row>
              </li>
              <li>
                <Row>
                  <Col className="align-self-center text-center h-100">
                    <Link to="/blocks/0">Genesis Block</Link>
                  </Col>
                </Row>
              </li>
              <li>
                <Row>
                  <Col className="align-self-center text-center h-100">
                    <a
                      href="//bitbucket.org/BlockchainChange"
                      target="_default"
                    >
                      Code Base Repository
                    </a>
                  </Col>
                </Row>
              </li>
            </ul>
          </Col>
          <Col sm={12} md={4} className="text-center">
            <h2 className="color5" style={lightShadow}>
              <strong>Wallet</strong>
            </h2>
            <ul className="list-unstyled">
              <li>
                <Row>
                  <Col className="align-self-center text-center h-100">
                    <Link to="/user/wallet/create">Create A Wallet </Link>
                  </Col>
                </Row>
              </li>
              <li>
                <Row>
                  <Col className="align-self-center text-center h-100">
                    <Link to="/user/sign-in">Sign In</Link>
                  </Col>
                </Row>
              </li>
              <li>
                <Row>
                  <Col className="align-self-center text-center h-100">
                    <a
                      href="//bitbucket.org/BlockchainChange/Wallet-Interface"
                      target="_default"
                    >
                      Wallet Interface Repository
                    </a>
                  </Col>
                </Row>
              </li>
            </ul>
          </Col>
          <Col className="text-center" sm={12} md={4}>
            <h2 className="color5" style={lightShadow}>
              <strong>More Info</strong>
            </h2>
            <ul className="list-unstyled">
              <li>
                <Row>
                  <Col className="align-self-center text-center h-100">
                    <a
                      href="//"
                      target="_default"
                    >
                      Whitepaper
                    </a>
                  </Col>
                </Row>
              </li>
              <li>
                <Row>
                  <Col className="align-self-center text-center h-100">
                    <Link
                      to="#Newsletter-SignUp"
                      onClick={() => {
                        alert("Newsletter Modal Popup goes here!");
                      }}
                    >
                      Newsletter Sign Up{" "}
                    </Link>
                  </Col>
                </Row>
              </li>
            </ul>
          </Col>
        </Row>
      </Navbar>,
      this.getCopyright(),
      <TermsOfService key="tos-modal" />,
      <PrivacyPolicy key="privacy-policy-modal" />,
      <AddressBook key="address-book-modal" />
    ];
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  };
};

export default withRouter(connect(mapStateToProps)(Footer));
