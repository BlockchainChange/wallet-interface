const nodeList = require('./node-list.json');
const HOST_URL = `http://${nodeList[0].address}:${nodeList[0].port}/api/v1`;
module.exports = HOST_URL;