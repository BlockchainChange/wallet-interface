import {
  GET_ADDRESS_BOOK,
  COPY_ADDRESS_TO_CLIPBOARD,
  ADD_ADDRESS_TO_ADDRESS_BOOK,
  REMOVE_ADDRESS_FROM_ADDRESS_BOOK,
  DELETE_ADDRESS_BOOK,
  DOWNLOAD_ADDRESS_BOOK
} from '../../actions';

export const getAddressBook = () => {
    let AddressBook = [];
    if(localStorage.getItem('AddressBook') !== null) {
      AddressBook = JSON.parse(localStorage.getItem('AddressBook'));      
    }
    return dispatch => {
        dispatch({
            type: GET_ADDRESS_BOOK,
            payload: AddressBook
        });
    };
};

export const addAddress = (nickname, receiver) => {
    let AddressBook = [];
    if(localStorage.getItem('AddressBook') === null) {
      AddressBook.push({nickname, receiver});
      localStorage.setItem('AddressBook', JSON.stringify(AddressBook));
    } else {
      AddressBook = JSON.parse(localStorage.getItem('AddressBook'));
      AddressBook.push({nickname, receiver});
      localStorage.setItem('AddressBook', JSON.stringify(AddressBook));      
    }
    return dispatch => {
        dispatch({
            type: ADD_ADDRESS_TO_ADDRESS_BOOK,
            payload: AddressBook
        });
    };
};

export const removeAddress = (nickname) => {
    let AddressBook = [];
    if(localStorage.getItem('AddressBook') !== null) {
        AddressBook = JSON.parse(localStorage.getItem('AddressBook'));
    }
    AddressBook = AddressBook.filter((address)=>{
      return address.nickname !== nickname;  
    });
    localStorage.setItem('AddressBook', JSON.stringify(AddressBook));
    return dispatch => {
        dispatch({
            type: REMOVE_ADDRESS_FROM_ADDRESS_BOOK,
            payload: AddressBook
        });
    };
};


export const copyAddressToClipboard = (address) => {
    
    return dispatch => {
        dispatch({
            type: COPY_ADDRESS_TO_CLIPBOARD,
            payload: address
        });
    };
};

export const downloadAddressBook = () => {
    const addressBook = [];
    return dispatch => {
        dispatch({
            type: DOWNLOAD_ADDRESS_BOOK,
            payload: addressBook
        });
    };
};

export const deleteAddressBook = () => {
    const addressBook = [];
    localStorage.setItem('AddressBook', JSON.stringify(addressBook));
    return dispatch => {
        dispatch({
            type: DELETE_ADDRESS_BOOK,
            payload: addressBook
        });
    };
};
