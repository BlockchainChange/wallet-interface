export const COPY_ADDRESS_TO_CLIPBOARD = 'COPY_ADDRESS_TO_CLIPBOARD';
export const GET_ADDRESS_BOOK = 'GET_ADDRESS_BOOK';
export const ADD_ADDRESS_TO_ADDRESS_BOOK = 'ADD_ADDRESS_TO_ADDRESS_BOOK';
export const REMOVE_ADDRESS_FROM_ADDRESS_BOOK = 'REMOVE_ADDRESS_FROM_ADDRESS_BOOK';
export const DOWNLOAD_ADDRESS_BOOK = 'DOWNLOAD_ADDRESS_BOOK';
export const DELETE_ADDRESS_BOOK = 'DELETE_ADDRESS_BOOK';