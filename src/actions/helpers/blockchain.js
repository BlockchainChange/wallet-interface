import axios from "axios";

import HOST_URL from "../../configs";
import { BLOCKCHAIN_ERROR, BLOCKCHAIN_LAST_FIFTEEN } from '../constants';
export const getBlockchain = () => {
  return dispatch => {
    axios
      .get(`${HOST_URL}/blocks`) //{headers:{Authorization:token}}
      .then(response => {
        dispatch({
          type: BLOCKCHAIN_LAST_FIFTEEN,
          payload: response
        });
      })
      .catch(err => {
        dispatch({
          type: BLOCKCHAIN_ERROR,
          payload: err
        });
      });
  };
};
