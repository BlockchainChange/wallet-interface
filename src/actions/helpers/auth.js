import Wallet from "../../controllers/wallet";

import {  USER_UNAUTHENTICATED, USER_AUTHENTICATED, AUTHENTICATION_ERROR } from '../constants';

export const authError = error => {
  return {
    type: AUTHENTICATION_ERROR,
    payload: error
  };
};

export const login = (privateKey, history) => {
  let tempWallet = new Wallet();
  if (privateKey !== null) {
    if (privateKey.substr(0, 27) !== "-----BEGIN PRIVATE KEY-----") {
      return dispatch => {
        dispatch(authError("Invalid Key found, please try again."));
      };
    }
    try {
      tempWallet.setPrivateKey(privateKey);
    } catch (error) {
      return dispatch => {
        dispatch(authError(error));
      };
    }
    if (tempWallet.key.isPrivate()) {
      return dispatch => {
        dispatch({
          type: USER_AUTHENTICATED,
          payload: tempWallet
        });
        history.push("/user/wallet");
      };
    } else {
      return dispatch => {
        dispatch(authError("Failed to load key."));
      };
    }
  } else {
    return dispatch => {
      dispatch(authError("Failed to load. Key missing."));
    };
  }
};

export const signOut = history => {
  return dispatch => {
    dispatch({
      type: USER_UNAUTHENTICATED,
      payload: {}
    });
  };
};
