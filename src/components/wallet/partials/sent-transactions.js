import React from 'react';
import { connect } from 'react-redux';
import { Card, CardHeader } from 'reactstrap';
import TransactionPagination from './transaction-pagination';
const SentTransactions = connect()((props) => {
        return (<Card className="mx-0 my-0">
                    <CardHeader className="card-header color7-bg" id="SentTransactionHeader">
                        <h3 className="mb-0 color4" data-toggle="collapse" data-target="#collapseSentTransaction" aria-expanded="true" aria-controls="collapseSentTransaction">
                            <strong className="display3">Sent</strong> <span className="float-right"> Total: {props.transactions.length ? props.transactions.length : '0'} </span>
                        </h3>
                    </CardHeader>
                    <div id="collapseSentTransaction" className="collapse hide" aria-labelledby="SentTransactionHeader">
                        { props.transactions.length ?
                          <TransactionPagination transactions={props.transactions} page={0} limit={15} received={false}/>
                          : ''}
                    </div>
                </Card>);
});
export default SentTransactions;