import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter} from 'react-router-dom';
import Header from './../header';
import Footer from './../footer';
import fileDownload from 'js-file-download';
import axios from 'axios';
import HOST_URL from '../../configs';
import SentTransactions from './partials/sent-transactions';
import ReceivedTransactions from './partials/received-transactions';
import AwardedBlocks from './partials/awarded-blocks';
import {
    Container,
    Card,
    CardHeader,
    CardBody,
    Row,
    Col
} from 'reactstrap';

const stylePointer = { cursor:"pointer"};


class WalletOverview extends Component {
    state = {
      balance:0,
      blocks:[],
      totalBlocks:0,
      includesGenesis:false,
      transactions:[],
      sentTransactions:[],
      receivedTransactions:[]
    };
 
    componentWillMount(){
      axios.get(`${HOST_URL}/wallet/balance`, {headers:{Authorization:JSON.stringify(this.props.auth.wallet.walletPublicKey)}})
        .then((balance) => {
          axios.get(`${HOST_URL}/wallet/transactions`, {headers:{Authorization:JSON.stringify(this.props.auth.wallet.walletPublicKey)}})
                .then((transactions) => {
                    axios.get(`${HOST_URL}/wallet/awards`, {headers:{Authorization:JSON.stringify(this.props.auth.wallet.walletPublicKey)}})
                      .then((response) => {
                        this.setState({
                         blocks:response.data.blocks,
                         totalBlocks:response.data.totalBlocks,
                         includesGenesis:response.data.includesGenesis,
                         balance:balance.data.balance,        
                         transactions:transactions.data.transactions,
                         sentTransactions: transactions.data.transactions.filter(transaction => transaction.sender.a === this.props.auth.wallet.walletPublicKey.a && transaction.sender.b === this.props.auth.wallet.walletPublicKey.b),
                         receivedTransactions: transactions.data.transactions.filter(transaction => transaction.receiver.a === this.props.auth.wallet.walletPublicKey.a && transaction.receiver.b === this.props.auth.wallet.walletPublicKey.b)
                        });
                      })
                      .catch(err => {console.log(err);});
                })
                .catch(err => {console.log(err);});
        })
        .catch(err => {
          console.log(err);
        });    
    }
    
    downloadImage(){
        // Grab the extension to resolve any image error
        const ext = this.qrImgBase64.split(';')[0].match(/jpeg|png|gif/)[0];
        // strip off the data: url prefix to get just the base64-encoded bytes
        const data = this.qrImgBase64.replace(/^data:image\/\w+;base64,/, "");
        const buf = new Buffer(data, 'base64');
        fileDownload(buf, 'image.' + ext);
    }
    
    render() {
      let imageQR = this.props.auth.wallet.getQRImage();
      let imgHtml = imageQR.split('/>');
      let base = imgHtml[0];
      this.qrImgBase64 = null;
      base = base.split(' ').reduce((s, v) => {
        if(v !== undefined){
          if(this.qrImgBase64 === null){
            let params = v.split('=');
            if(params[0] === 'src') {
                this.qrImgBase64 = params[1].substr(1,params[1].length - 2); 
                return this.qrImgBase64;            
            }
          }else{
            return s;
          }
        }
        return s;
      });
      this.qrImgBase64 = base;
      let imgFrontLink = [imgHtml.shift()];
      imgFrontLink.push(' class="img-qr-code"');
      imgFrontLink.push('/>');
      imgFrontLink.push(`<div class="qr-text"> Click to Download </div>`);
      imageQR = imgFrontLink.join('');
      return ([<Header key="header"/>,
            <section key="body">
                <Container fluid>
                    <Container>
                        <Row>
                            <Col className="text-center">
                                <h1 style={{textShadow: '2px 2px 2px #007bff'}}>Wallet Explorer</h1>
                            </Col>
                        </Row>
                        <Card>
                            <CardHeader className="text-center px-2 py-4">
                                <Row>
                                    <Col size={12} sm={12} md={3} lg={3} className="align-self-center">
                                        <h2 className="pt-0 pb-2 text-center"><strong>Balance</strong></h2>
                                        <h2><small>{this.state.balance ? this.state.balance: 0} coin</small></h2>
                                    </Col>
                                    <Col size={12} sm={12} md={6} lg={6} className="my-0 py-0 px-1 align-self-center justify-content-center">
                                        <h3>Public Key</h3>
                                        <pre className="pt-2" style={stylePointer} title="Click to Copy to Clipboard" onClick={(e)=>{
                                          var textField = document.createElement('textarea');
                                          textField.innerText = e.target.innerText;
                                          document.body.appendChild(textField);
                                          textField.select();
                                          document.execCommand('copy');
                                          textField.remove();                                          
                                          }}>{this.props ? this.props.auth.wallet.walletPublicKey.a + '\n' + this.props.auth.wallet.walletPublicKey.b: ''}</pre>
                                    </Col>
                                    <Col size={12} sm={12} md={3} lg={3} className="align-self-center pr-0">
                                        <button className="btn btn-sm btn-link" onClick={this.downloadImage.bind(this)}>
                                            <div className="thumbnail" dangerouslySetInnerHTML={{ __html: imageQR }} />
                                        </button>
                                    </Col>
                              </Row>
                            </CardHeader>
                            <CardBody className="mx-0 my-0 px-0 py-0">
                                <Row className="justify-content-center no-gutters">
                                    <Col size={12} sm={12} md={6} lg={6}>
                                      { this.state.sentTransactions ?
                                          <SentTransactions transactions={this.state.sentTransactions} />
                                          : ''}
                                    </Col>
                                    <Col size={12} sm={12} md={6} lg={6}>
                                      { this.state.receivedTransactions ?
                                      <ReceivedTransactions transactions={this.state.receivedTransactions} />
                                        : ''}
                                    </Col>
                                    <Col>
                                      <AwardedBlocks blocks={this.state.blocks || []}  totalBlocks={this.state.totalBlocks || 'N/A'}/>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Container>
                </Container>
            </section>,
      <Footer key="footer"/>]);
    }
  }
  
  const mapStateToProps = state => {
  // add spent tansactions and unspent transactions
  return {
    auth: state.auth,
    error: state.auth.error
  };
};

export default withRouter(connect(mapStateToProps)(WalletOverview));
