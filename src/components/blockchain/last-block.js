import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import Header from '../header';
import Footer from '../footer';

import HOST_URL from '../../configs';
class LastBlock extends Component {
    componentDidMount() {
        this.getLastBlock();
    }
    getLastBlock() {
      axios
        .get(`${HOST_URL}/block/last-block`)
        .then((response) => {
          this.setState({block:response.data});          
        })
        .catch((err) => {
          console.log(err);
          return 0;
        });
    }
    render() {
      return (
          [<Header key="header"/>,
          this.state ? 
                <section key="body">
                  <h2>Block : {this.state.block.blockId} Forged By: {this.state.block.forgedBy}</h2>
                  <p>Award: {this.state.block.awardTotal}<br/> </p>
                  Awarded To: <pre>{this.state.block.awardWallet}</pre>
                  <p>Previous Hash : <Link to={`/blocks/${this.state.block.blockId - 1}`}><code>{this.state.block.previousHash}</code></Link></p>
                  <p>Hash : <code>{this.state.block.hash}</code></p>
                  <p>Difficulty: {this.state.block.difficulty} TimeComplexity: {this.state.block.timeComplexity} Proof: {this.state.block.proof} Timestamp:{this.state.block.timestamp} </p>
                  <h3><strong>Transactions :</strong></h3>
                  {this.state.block.transactions ? <ul> {this.state.block.transactions.map(transaction =>
                      <li>
                          <h4>Transaction Index : <Link to={`/transactions/${transaction.transactionsIndex}`}>{transaction.transactionsIndex}</Link></h4> <br/>
                          <strong>Sender : </strong>  <pre>{transaction.sender}</pre> <br/>
                          <strong>Receiver : </strong>  <pre>{transaction.receiver}</pre> <br/>
                          amount: {transaction.amount} keys<br/>
                          Previous Hash :<Link to={`/transactions/${transaction.transactionsIndex - 1}`}><code>{transaction.txPreviousHash}</code></Link> <br/>
                          Hash : <code>{transaction.txHash}</code>
                          <p>forged : {transaction.forged ? 'true' : 'false'}</p>
                      </li>
                  )}</ul> :''}
                </section>
          : '',
          <Footer key="footer" />]
      );
    }
  }
const mapStateToProps = state => {
  return {
    auth: state.auth,
    chain: state.chain
  };
};
export default withRouter(connect(mapStateToProps)(LastBlock));