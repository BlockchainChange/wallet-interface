import {
    CURRENT_BLOCK_LOOKUP_ERROR,
    GET_CURRENT_BLOCK,
  } from '../actions';
  
export default (currentBlock = 0, action) => {
    switch (action.type) {
      case GET_CURRENT_BLOCK:
        return action.payload;
      case CURRENT_BLOCK_LOOKUP_ERROR:
        return action.payload;
      default:
        return currentBlock;
    }
};