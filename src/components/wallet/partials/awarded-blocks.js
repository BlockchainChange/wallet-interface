import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
Card,
CardHeader,
CardBody,
Row,
Col
} from 'reactstrap';

export default connect()((props) => {
        return (
                <Card>
                    <CardHeader id="headingAwards">
                        <h3 className="mb-0" data-toggle="collapse" data-target="#collapseAwards" aria-expanded="true" aria-controls="collapseAwards">
                            <strong className="display3">Awarded</strong> <span className="float-right">Total: {props.totalBlocks}  <small>BLOCKS</small></span>
                        </h3>
                    </CardHeader>
                    <div id="collapseAwards" className="collapse hide" aria-labelledby="headingAwards" >
                        <CardBody className="mx-0 my-0 px-0 py-0">
                        {props.blocks ? props.blocks.map((block) => {
                                return (<Card key={`Awards-BlockId-${block.blockId}`}>
                                    <CardHeader id="heading-level-awards">
                                        <Row data-toggle="collapse" data-target="#collapse-level-awards" aria-expanded="true" aria-controls="collapse-level-awards">
                                            <Col><small>ID {block.blockId}</small></Col>
                                            <Col><small>AWARD {block.awardTotal}</small></Col>
                                            <Col><small>COUNT {block.transactions.length}</small></Col>
                                            <Col><small>{ block.transactions.length ? '0' : '0' } TOTAL</small></Col>
                                            <Col><small>{ block.transactions.length ? '0' : '0' } FEES </small> <span className="float-right"><Link to={`/blocks/${block.blockId}`} ><i className="fa fa-search"></i></Link></span></Col>
                                        </Row>
                                    </CardHeader>
                                </Card>)}) : ''} 

                        </CardBody>
                    </div>
                </Card>);
})