import React, {Component} from 'react';
import { withRouter, Link } from 'react-router-dom';
import Header from './header';
import Footer from './footer';
import {
    Container,
    Row,
    Col,
    Jumbotron
} from 'reactstrap';

class Not_Found extends Component {
    render() { 
        return(
            [<Header key="header" />,
            <section key="body">
                <Container fluid>
                    <Container>
                        <Row>
                            <Col className="text-center">
                                <Jumbotron>
                                    <h1 style={{textShadow: '2px 2px 2px #007bff'}}>Not Found</h1>
                                    <p>The resource my have expired or does not exist, try going back or navigating to another page.</p>
                                    <p>If you continue to have this issue or think this could be a bug let us know <Link to="/contact-us" title="Contact us for more details.">Contact Us</Link></p>
                                </Jumbotron>
                            </Col>
                        </Row>
                    </Container>
                </Container>
            </section>,
            <Footer key="footer"/>]);
    }
}

export default withRouter(Not_Found);
