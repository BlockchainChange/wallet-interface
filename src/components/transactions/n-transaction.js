import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm, Field, change as changeFieldValue } from 'redux-form';
import { Container, Row, Col, Card, CardHeader, CardBody,
Nav, NavLink, NavItem, TabContent,
TabPane, Button, ButtonGroup, ButtonToolbar, Badge,
InputGroup, InputGroupAddon } from 'reactstrap';
import Header from '../header';
import Footer from '../footer';
import { getTransactionN } from '../../actions';
import crypto from 'crypto';
import Wallet from '../../controllers/wallet';
import bcrypt from "bcryptjs";
const stylePointer = { cursor:"pointer"};
class NTransaction extends Component {
    constructor(props){
      super(props);
      this.state = {
        activeTab : '1',
        transaction  : props.transaction,
        auth : props.auth
      };
      this.handleSubmit = props.handleSubmit.bind(this);
      
    }
    
    validateHash () {
      let validTransaction = {
        transactionsIndex: this.props.transaction.transactionsIndex,
        listingHost: this.props.transaction.listingHost,
        forged: false,
        fee: 0, //get fee from listing node.
        txPreviousHash : this.props.transaction.txPreviousHash
      };
      this.setState({
        validatedHash : bcrypt.compareSync(JSON.stringify(validTransaction) + 3 + this.props.transaction.proof + 2, this.props.transaction.txHash)
        }); 
    }
    
    validateSignature() {
        const wallet = new Wallet();
        console.log('Start transaction signature validation');
        const validTransaction = {
            sender : this.props.transaction.sender,
            amount : this.props.transaction.amount,
            stake : this.props.transaction.stake,
            receiver : this.props.transaction.receiver,
            data : [this.props.transaction.data[0]],
            timestamp : this.props.transaction.timestamp,
            txPreviousHash : this.props.transaction.txPreviousHash,
            signature : this.props.transaction.signature
        };
        const validatedTransaction = wallet.verifySignature(validTransaction);
        if(!validatedTransaction){
            console.log('Error :: Invalid transaction signature');
        } else {
            console.log('Signature verified.');
        }
        this.setState({
          validatedSignature : validatedTransaction
          });
    }
    
    renderAlert() {
      if (!this.props.error) return null;
      return (
          <div className="alert alert-danger" role="alert">
              <h3 className="color14">{this.props.error}</h3>
          </div>   
      );
    }
  
    renderCipherSelect(button) {
      return(<form onSubmit={this.handleSubmit(this.decryptMessage.bind(this))}>
              <Row className="align-self-center align-content-center">
                <Col md={5} className="px-0">
                    <Field component="select" name="cipherAlgo" className="form-control input-sm">{this.renderCipherOptions()}</Field>
                </Col>
                <Col md={7} className="px-0">
                    <InputGroup>                
                        <Field component="input" type="text" name="passphrase" placeholder="Enter a Passphrase" className="form-control input-sm" aria-label="passphrase"/>
                        <InputGroupAddon addonType="prepend">
                          <Button type="submit">
                            <i className="fa fa-unlock"></i>
                          </Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Col>
              </Row>
            </form>);
              
    }
    
    decryptMessage(formElements) {
        let { cipherAlgo, passphrase } = formElements;
        if(cipherAlgo === undefined) {
          cipherAlgo = 'des-ecb';
        }
        if(passphrase === undefined) {
          this.setState({ error : 'Passphrase missing, and required to decrypt.'});          
          return;
        }        
        const decipher = crypto.createDecipher(cipherAlgo, passphrase);
        
        let decrypted = decipher.update(this.props.transaction.data[0].message, 'hex', 'utf8');
        try {
            decrypted += decipher.final('utf8');  
        } catch (error){
          this.setState({error});
          return undefined;
        }
        const transaction = this.props.transaction;
        transaction.data[0].message = decrypted;
        this.setState({transaction, decrypted:true});
    }
    
    renderCipherOptions(){
      return crypto.getCiphers().map((cipher,i) => {
        if(i === 0)
            return <option key={i} defaultValue={cipher} >{cipher}</option>;
        return <option key={i} value={cipher}>{cipher}</option>;
      });
    }
    
    toggle(tab) {
      if (this.state.activeTab !== tab) {
        this.setState({
          activeTab: tab
        });
      }
    }
    
    componentWillMount() {
      this.props.getTransactionN( this.props.match.params.transactionIndex);
    }
    
    render() {
      return (
            [<Header key="header"/>,
            this.props.transaction.transactionsIndex !== undefined ?
          <section key="body">
           <Container fluid>
              <Container className="color16">
                  <Row>
                    <Col size={8} className="px-0"><h3><strong>Transactions Index </strong> <Badge className="">{this.props.transaction.transactionsIndex}</Badge></h3></Col>
                    <Col size={1} className="px-0 align-self-center text-center"><h3><strong>Amount </strong> <Badge>{this.props.transaction.amount} <span className="color6">coin</span></Badge></h3></Col>
                    <Col size={1} className="px-0 align-self-center text-center"><h3><strong>Proof </strong> <Badge>{this.props.transaction.proof}</Badge></h3></Col>
                    <Col size={2} className="px-0"><h3><span className="float-right"><strong>Forged</strong> {this.props.transaction.forged ? <Badge color="success">True</Badge> : <Badge color="success">False</Badge>}</span></h3></Col>
                  </Row>
                  <Row>
                    <Col md={12} lg={6} className="text-center">
                      <h5 className="text-left"> <strong>Hash</strong> <span className="float-right">
                      <Badge style={{cursor:"pointer"}} title="Click to validate this transactions Hash." onClick={this.validateHash.bind(this)}>Validate Hash <i className={this.state.validatedHash === undefined ? 'fa fa-question' : this.state.validatedHash ? 'text-success fa fa-check' : 'text-danger fa fa-close'} ></i></Badge>
                      <Badge style={{cursor:"pointer"}} title="Click to validate this transactions Signature." onClick={this.validateSignature.bind(this)}>Validate Signature <i className={this.state.validatedSignature === undefined ? 'fa fa-question' : this.state.validatedSignature ? 'text-success fa fa-check' : 'text-danger fa fa-close'}></i></Badge>
                      </span></h5>
                        <code>{this.props.transaction.txHash.substr(7)}</code>
                    </Col>
                    <Col md={12} lg={6} className="text-center">
                      <h5 className="text-left"><strong>Previous Hash</strong></h5>
                      <Badge color="secondary" className="mt-0 pt-0"><small>Click <i className="fa fa-arrow-right"></i> </small> </Badge>&nbsp;
                      <NavLink href={`/transactions/${Number(this.props.transaction.transactionsIndex) - 1}`}><code>{this.props.transaction.txPreviousHash.substr(7)}</code></NavLink>
                    </Col>
                  </Row>
                  <Row>
                  
                    <Col sm={12} md={6}>
                      <strong>Sender : </strong>
                      <Card className="align-self-center color18-bg">
                      {this.state.auth.wallet ? 
                        <CardHeader>
                          <ButtonToolbar>
                            <ButtonGroup>
                              <Button className="btn-sm">Add To Address Book <i className="fa fa-address-book"></i></Button>
                            </ButtonGroup>
                          </ButtonToolbar>
                        </CardHeader> : ''}
                        <CardBody className="text-center px-0 align-self-center">
                          <pre className="color3 mx-0 my-0" style={stylePointer} title="Click to Copy to Clipboard" onClick={(e)=>{
                                          var textField = document.createElement('textarea');
                                          textField.innerText = e.target.innerText;
                                          document.body.appendChild(textField);
                                          textField.select();
                                          document.execCommand('copy');
                                          textField.remove();                                          
                                          }}>{this.props.transaction.sender.a}<br/>{this.props.transaction.sender.b}</pre>
                        </CardBody>
                      </Card>
                    </Col>
  
                    <Col  sm={12} md={6}>
                      <strong>Receiver : </strong>
                      <Card className="align-self-center color18-bg">
                      {this.state.auth.wallet ? 
                        <CardHeader>
                          <ButtonToolbar>
                            <ButtonGroup>
                              <Button className="btn-sm">Add To Address Book <i className="fa fa-address-book"></i></Button>
                            </ButtonGroup>
                          </ButtonToolbar>
                        </CardHeader> : ''}
                        <CardBody className="text-center px-0 align-self-center">
                          <pre className="color3 mx-0 my-0" style={stylePointer} title="Click to Copy to Clipboard" onClick={(e)=>{
                                          var textField = document.createElement('textarea');
                                          textField.innerText = e.target.innerText;
                                          document.body.appendChild(textField);
                                          textField.select();
                                          document.execCommand('copy');
                                          textField.remove();                                          
                                          }}>{this.props.transaction.receiver.a}<br/>{this.props.transaction.receiver.b}</pre>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                  
                  <Row className="pt-2">
                    <Col>
                      <Nav tabs role="tablist">
                      {(this.props.transaction.data[0].message) ?
                        <NavItem>
                          <NavLink className={{ active: this.state.activeTab === '1' }} onClick={() => { this.toggle('1'); }}>Message</NavLink>
                        </NavItem>
                        : ''}
                        <NavItem>
                          <NavLink className={{ active: this.state.activeTab === '2' }} onClick={() => { this.toggle('2'); }}>Data-Segments</NavLink>
                        </NavItem>
                      </Nav>
                      
                      <TabContent  activeTab={this.state.activeTab}>
                          {this.props.transaction.data[0].message ?
                            <TabPane fade="fade" tabId="1">
                              <Card className="align-self-center color18-bg">
                                <CardBody className="px-0 align-self-center text-white">
                                  {this.renderAlert()}           
                                  {this.props.transaction.data[0].message && !this.state.decrypted ? this.renderCipherSelect() : ''}
                                  <pre className="mx-0 my-0" ref="message">{this.props.transaction.data[0].message}</pre>
                                </CardBody>                            
                              </Card>
                            </TabPane>
                          : ''}
                        <TabPane fade="fade" tabId={this.props.transaction.data[0].message ? '2' : '1'}>
                          <Card className="align-self-center color16-bg">
                            <CardBody className="px-0 align-self-center text-white">
                                Storing public transaction on the blockchain.
                            </CardBody>
                          </Card>
                        </TabPane>
                      </TabContent>                      
                    </Col>
                  </Row>
              </Container>
           </Container>
          </section>
         : <div className=" card align-self-center color14-bg">
            <div className="card-body px-0 align-self-center text-white">
                The Transaction cannot be reached or are loading.
            </div>
          </div>,
         <Footer key="footer" />]
      );
    }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ changeFieldValue, getTransactionN }, dispatch);
}

const mapStateToProps = state => {
  return {
    auth:state.auth,
    transaction : {...state.transaction}
  };
};
export default withRouter(reduxForm({
  form: 'decrypt-message',
  fields: ['passphrase','cipherAlgo']
})(connect(mapStateToProps, mapDispatchToProps)(NTransaction)));