import React, { Component } from 'react';
import { connect } from 'react-redux';
import Wallet from '../../controllers/wallet';
import Header from '../header';
import Footer from '../footer';
import fileDownload from 'js-file-download';
import { 
  Container,
  Row,
  Col, 
  Jumbotron,
  Button,
  UncontrolledAlert
} from 'reactstrap';

class CreateWallet extends Component {
  constructor(props){
    super(props);
    this.state = {
      wallet :  null
    };
  }

  saveToLocalStorage () {
    localStorage.setItem('privateKey', this.state.wallet.walletPrivateKey);
  }

  downloadFile() {
    fileDownload(this.state.wallet.walletPrivateKey, 'PrivateWalletKey.pem');
  }

  createWallet() {
    const newWallet = new Wallet();
    newWallet.generateKeyPair();
    this.setState({
      wallet :  newWallet
    });
  }

  render() {
    return (
      [<Header key="header"/>,
      <section key="body">
        <Container fluid>
            {this.state && this.state.wallet ?
              <Container>
                <Row>
                  <Col className="text-center">
                    <h1>Save Your  Wallet Details</h1>
                    <UncontrolledAlert><strong style={{textShadow: '2px 2px 4px black'}}>Note : </strong> 
                    A private key (PEM file) has been generated.  Download this wallet key and store it in a safe place.</UncontrolledAlert>
                    <p>Without your private key, you <strong className="text-danger">WILL NOT</strong> be able to access your wallet.</p>
                  </Col>
                </Row>
                <Jumbotron>
           				<h3 className="display-3 pb-4" style={{textShadow: '2px 2px 4px black'}}>PUBLIC KEY</h3>
                  <Row>
                    <Col md={4} className="align-self-center">
                        <Col sm={3} md={2} lg={2} className="mx-auto">
                          <div className="thumbnail" dangerouslySetInnerHTML={{ __html: this.state.wallet.getQRImage() }} />
                        </Col>
                    </Col>
                    <Col md={6} className="text-center">
                        <pre>
                         {this.state.wallet.walletPublicKey.a}<br/> {this.state.wallet.walletPublicKey.b}
                        </pre>
                    </Col>
                  </Row>
                </Jumbotron>                
                <Jumbotron className="mx-auto">
                  <Row>
           					<Col md={12} className="align-self-center">
                        <Row>
                          <Col>
                            <h3 className="display-3 pb-4" style={{textShadow: '2px 2px 4px black'}}>PRIVATE KEY</h3>
                          </Col>
                        </Row>
                        <Row className="align-content-center">
                          <Col md={3} sm={8} className="align-self-center mx-auto">
                            <Button size="lg" block onClick={this.downloadFile.bind(this)} >Download Key</Button>
                          </Col>
                          <Col md={3} sm={8} className="align-self-center mx-auto">
                            <Button size="lg" block onClick={this.saveToLocalStorage.bind(this)}>Save To Browser</Button>
                          </Col>
                        </Row>
                    </Col>
                  </Row>
                </Jumbotron>
              </Container>
          :
          <Container>
            <Row>
              <Col className="text-center">
                <UncontrolledAlert color="info"><strong style={{textShadow: '2px 2px 4px black'}}>Note : </strong> Don&#39;t lose your wallet key! You want to store your private key in a safe place for accessing your wallet.</UncontrolledAlert>
              </Col>
            </Row>
            <Row>
              <Col className="text-center">
                <h1>Create Your <span style={{textShadow: '2px 2px 4px black'}}>Private wallet</span></h1>
              </Col>
            </Row>
            <Row className="text-center py-3">
              <Col>
                <Button size="lg" outline color="info" onClick={this.createWallet.bind(this)}>Get Started</Button>
              </Col>
            </Row>
          </Container>
            }
        </Container>
      </section>,
      <Footer key="footer"/>]
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.auth.error
  };
};

CreateWallet = connect(mapStateToProps)(CreateWallet);

export default CreateWallet