import axios from "axios";

import Wallet from "../../controllers/wallet";

import HOST_URL from "../../configs";

import bcrypt from "bcryptjs";

import {
  MISSING_PARAMETER,
  TRANSACTION_REJECTED,
  TRANSACTION_RECORDED,
  LOOKUP_ERROR,
  CONNECTION_ERROR,
  TRANSACTION_LOOKUP,
  LAST_TRANSACTION_LOOKUP_ERROR
} from '../constants';


export const getLastTransaction = () => {
  return dispatch => {
    axios
      .get(`${HOST_URL}/transactions/last-transaction`)
      .then(lastTransaction => {
        dispatch({
          type: TRANSACTION_LOOKUP,
          payload: lastTransaction.data
        });
      })
      .catch(error => {
        dispatch({
          type: LAST_TRANSACTION_LOOKUP_ERROR,
          payload: error
        });
      });
  };
};

export const signTransaction = () => {};

export const hashTransaction = () => {};

export const sendTransaction = (
  receiver,
  sender,
  amount,
  stake,
  message,
  history
) => {
  return dispatch => {
    if (!receiver || !sender) {
      return dispatch({
        type: MISSING_PARAMETER,
        payload: "A parameter that was required is missing."
      });
    }

    let receiverWalletAddress = receiver.split(" ");
    receiverWalletAddress = {
      a: receiverWalletAddress[0],
      b: receiverWalletAddress[1]
    };
    const receiverWallet = new Wallet();
    try {
      receiverWallet.setPublicKey(receiverWalletAddress);
    } catch (err) {
      return dispatch({
        type: TRANSACTION_REJECTED,
        payload: err
      });
    }
    if (!receiverWallet.key.isPublic()) {
      return dispatch({
        type: TRANSACTION_REJECTED,
        payload: "Invalid receiver wallet."
      });
    }

    if (message === undefined) {
      message = null;
    }
    /* get spending transaction */
    let validTransaction = {
      transactionsIndex: 0,
      listingHost: HOST_URL,
      forged: false,
      fee: 0 //get fee from listing node.
    };
    console.log(validTransaction);
    axios
      .get(`${HOST_URL}/transactions/last-transaction`)
      .then(lastTransaction => {
        validTransaction.txPreviousHash =
          lastTransaction.data.transactions[0].txHash;
        validTransaction.transactionsIndex =
          lastTransaction.data.transactions[0].transactionsIndex + 1;
        const transaction = sender.signTransaction(
          amount,
          receiverWallet.walletPublicKey,
          validTransaction.txPreviousHash,
          message,
          stake
        );
        validTransaction = Object.assign(validTransaction, transaction);
        let proof = 0;
        validTransaction.txHash = bcrypt.hashSync(
          JSON.stringify(validTransaction) + 3 + proof + 2,
          1
        );
        console.log("Hash Before : ", validTransaction.txHash);
        while (validTransaction.txHash.substr(7, 2) !== "KE") {
          proof++;
          validTransaction.txHash = bcrypt.hashSync(
            JSON.stringify(validTransaction) + 3 + proof + 2,
            1
          );
          // Add this to the state
          console.log("Hash : ", validTransaction.txHash);
        }
        validTransaction.proof = proof;
        console.log("Hash After : ", validTransaction.txHash);
        axios
          .post(
            `${HOST_URL}/transactions`,
            { transaction: validTransaction },
            {
              headers: { Authorization: JSON.stringify(sender.walletPublicKey) }
            }
          )
          .then(response => {
            console.log(response);
            dispatch({
              type: TRANSACTION_RECORDED,
              payload: response.data
            });
            history.push("/");
          })
          .catch(error => {
            dispatch({
              type: TRANSACTION_REJECTED,
              payload: error
            });
          });
      })
      .catch(error => {
        dispatch({
          type: CONNECTION_ERROR,
          payload: error
        });
      });
  };
};


export const getTransactionN = (transactionsIndex) => {
  return dispatch => {
    axios
      .get(`${HOST_URL}/transactions/${transactionsIndex}`)
      .then((response) => {
          dispatch({
            type : TRANSACTION_LOOKUP,
            payload : response.data.transaction
          });
      })
      .catch((error) => {
        dispatch({
            type: LOOKUP_ERROR,
            payload: error
          });
      });
  };  
};