import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import Header from "../header";
import Footer from "../footer";
import { getBlockchain, getCirculation, getCurrentBlock } from "../../actions";
import {
  Container,
  Row,
  Col,
  Badge,
  Card,
  CardHeader,
  CardBody,
  Jumbotron,
} from "reactstrap";
class ChainOverview extends Component {
  componentWillMount() {
    this.props.getBlockchain();
    this.props.getCirculation();
    this.props.getCurrentBlock();
  }
  render() {
    return [
      <Header key="header" />,
      <Container fluid key="body">
        <Container>
          <Row>
            <Col className="text-center">
              <h1 style={{ textShadow: "2px 2px 2px #007bff" }}>
                Blockchain Explorer
              </h1>
            </Col>
          </Row>
          <Row>
            <Col md={5} sm={12} className="mx-auto align-self-center my-1">
              <h5>
                <strong>Circulation : </strong>{" "}
                <br className="d-block d-sm-none" />
                {this.props.circulation.sum.toFixed(6)} coin
              </h5>
            </Col>
            <Col md={4} sm={12} className="mx-auto align-self-center">
              <h5>
                <strong>Last Block</strong> : <br className="d-sm-none" />
                {this.props.currentBlock}
              </h5>
            </Col>
            <Col md={3} sm={12} className="d-block mx-auto align-self-top">
              <Row className="bg-secondary">
                <Col className="bg-secondary">
                  POW{" "}
                  <Badge color="success" className="float-right mt-1">
                    Active
                  </Badge>
                </Col>
                <Col className="bg-info">
                  POS{" "}
                  <Badge color="warning" className="float-right mt-1">
                    0 %
                  </Badge>
                </Col>
              </Row>
            </Col>
          </Row>
          <ul className="list-unstyled p-0 m-0" id="accordion">
            {this.props.chain.length
              ? this.props.chain.map((block, i) => (
                  <li key={block.blockId}>
                    <Card>
                      <CardHeader
                        className="p-0"
                        id={`header_${block.blockId}`}
                        data-toggle="collapse"
                        data-target={"#collapse_" + block.blockId}
                        aria-expanded={i === 0 ? true : false}
                        aria-controls={`collapse_${block.blockId}`}
                      >
                        <Row className="no-gutters justify-content-center">
                          <Col className="align-self-center" md={3}>
                            <Col>
                              <strong>Block</strong>{" "}
                              {block.blockId <= 0 ? (
                                <Link to={`/blocks/${0}`}>{0}</Link>
                              ) : (
                                <Link to={`/blocks/${block.blockId}`}>
                                  {block.blockId}
                                </Link>
                              )}
                            </Col>
                            <Col>
                              <strong>Award </strong> {block.awardTotal}
                            </Col>
                          </Col>

                          <Col className="align-self-center">
                            <Row className="no-gutters justify-content-center">
                              <Col className="align-self-center">
                                <Jumbotron className="py-2 mb-0 text-center bg-white">
                                  <strong className="display-5">
                                    Forged By: {block.forgedBy}
                                  </strong>
                                </Jumbotron>
                              </Col>
                            </Row>
                          </Col>

                          <Col
                            className="align-self-center text-center mx-auto"
                            md={3}
                          >
                            <strong>
                              {new Date(block.timestamp).toUTCString()}
                            </strong>
                          </Col>
                        </Row>
                      </CardHeader>
                      <div
                        id={"collapse_" + block.blockId}
                        className={i === 0 ? "collapse show" : "collapse"}
                        aria-labelledby={`header_${block.blockId}`}
                        data-parent="#accordion"
                      >
                        <CardBody className="align-self-center">
                          <Row>
                            <Col>
                              <h3>
                                <strong>TimeComplexity</strong> :{" "}
                                {block.timeComplexity}
                              </h3>
                            </Col>
                            <Col className="self-align-center text-center">
                              <h3>
                                <strong>Proof</strong> : {block.proof}
                              </h3>
                            </Col>
                            <Col className="text-right">
                              <h3>
                                <strong>Difficulty</strong> : {block.difficulty}
                              </h3>
                            </Col>
                          </Row>
                          <Row className="justify-content-center">
                            <Col className="align-self-center ml-4 bg-light">
                              <h3>
                                <strong>Awarded To:</strong>
                              </h3>
                              <pre>
                                {block.awardWallet.a}
                                <br />
                                {block.awardWallet.b}
                              </pre>
                            </Col>
                            <Col>
                              <h3>
                                <strong>Hash : </strong>
                              </h3>
                              <p>
                                <code>{block.hash.substr(6)}</code>
                              </p>
                              <h3>
                                <strong>Previous Hash : </strong>
                              </h3>
                              <p>
                                <Link to={`/blocks/${block.blockId - 1}`}>
                                  <code>{block.previousHash.substr(6)}</code>
                                </Link>
                              </p>
                            </Col>
                          </Row>
                        </CardBody>
                      </div>
                    </Card>
                  </li>
                ))
              : ""}
          </ul>
        </Container>
      </Container>,
      <Footer key="footer" />,
    ];
  }
}
const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    chain: state.chain,
    circulation: state.circulation,
    currentBlock: state.currentBlock,
  };
};
export default withRouter(
  connect(mapStateToProps, { getBlockchain, getCirculation, getCurrentBlock })(
    ChainOverview
  )
);
