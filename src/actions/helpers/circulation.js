import axios from "axios";

import HOST_URL from "../../configs";

import { LOOKUP_ERROR, GET_CIRCULATION, GET_CIRCULATION_LOOKUP_ERROR } from '../constants';

export const getCirculation = () => {
  return dispatch => {
    axios
      .get(`${HOST_URL}/blocks/circulation-totals`)
      .then(response => {
        if (response.status === 200) {
          return dispatch({
            type: GET_CIRCULATION,
            payload: response.data[0].sum
          });
        } else {
          return dispatch({
            type: LOOKUP_ERROR,
            payload: "Error loading Circulation Totals"
          });
        }
      })
      .catch(err => {
        return dispatch({
          type: GET_CIRCULATION_LOOKUP_ERROR,
          payload: err
        });
      });
  };
};
