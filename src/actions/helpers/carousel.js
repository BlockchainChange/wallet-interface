import { GET_CAROUSEL_ITEMS } from '../constants';

export const getItems = (carousels) => {   
    return dispatch => {
        dispatch({
            type: GET_CAROUSEL_ITEMS,
            payload: carousels
        });
    };
}