import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm, Field, change as changeFieldValue } from 'redux-form';
import { getLastTransaction, signTransaction, hashTransaction, sendTransaction } from '../../actions';
import Header from '../header';
import Footer from '../footer';
import crypto from 'crypto';
import QrReader from 'react-qr-reader';
import { 
  Container,
  Row,
  Col,
  FormGroup,
  InputGroup,
  InputGroupText,
  UncontrolledAlert,
  Button
} from 'reactstrap';
class SendTransaction extends Component {
  constructor(props){
    super(props);
    this.state = {
        error: props.error,
        progress:props.progress,
        users: props.users,
        transaction:props.transaction,
        auth:props.auth,
        hash: props.transaction.txHash,
        encrypted: false,
        delay: 300,
        result: 'No result',
        showCamera: false,
      };
    this.handleGeneratePassphrase.bind(this);
    this.renderQRReader = this.renderQRReader.bind(this);
    this.handleScan = this.handleScan.bind(this);
    this.handleSubmit = props.handleSubmit.bind(this);
  }
  handleGeneratePassphrase() {
      this.refs.saltSpinner.classList.add('fa-spin');
      this.props.changeFieldValue(this.props.form, 'passphrase', crypto.randomBytes(16).toString('hex'));
      setTimeout(()=>{this.refs.saltSpinner.classList.remove('fa-spin');}, 1000);      
  }
  
  checkMessageLength(message) {
    if(message !== undefined){
      if(message.length < 255) return false;
      return true;
    }
    return true;
  }
  
  handleFormSubmit(formElements){
    let { receiver, amount, stakeAmount, message } = formElements;
    const { auth, history } = this.props;
    if( auth === undefined || auth.wallet === undefined) {
      this.setState({
        progress: 'error',
        error:'Error :: Authentication is required to proceed.'
      });
    }
    if(receiver.length < 128) {
      this.setState({
        progress: 'error',
        error:'Max message length is a total off 255 characters.'
      });
    }
    if(this.checkMessageLength(message)){
      this.setState({
        progress: 'Error',
        error:'Error :: Message length is a max total of 255 characters.'
      });
    }
    if(this.state.encrypted) {
        let { cipherAlgo , passphrase } = formElements;
        if(cipherAlgo === undefined) cipherAlgo = 'des-ecb';
        console.log('wallet enc', cipherAlgo, passphrase);
        const cipher = crypto.createCipher(cipherAlgo, passphrase);
        let crypted = cipher.update(message, 'utf-8', 'hex');
        crypted += cipher.final('hex');
        message = crypted;
        if(this.checkMessageLength(message)){          
          this.setState({
            progress: 'Error',
            error:'Error :: Message length is a max total of 255 characters after encryption.'
          });
        }
        /*
        const decipher = crypto.createDecipher(cipherAlgo, passphrase);
        let decrypted = decipher.update(message, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        console.log('decrypted', decrypted);
        */
    }
    if(this.state.progress === undefined){
      this.setState({progress:'Sending'});
      this.props.sendTransaction(receiver, auth.wallet, Number(amount).toFixed(6), stakeAmount, message, history);   
    }
  }
  
  resetForm() {
    this.setState({ progress:undefined, error:undefined, showCamera:false, encrypted:false });
  }
  
  renderEncryptedState(){
     return this.state.encrypted ? <small>Encrypted  : </small> : <small>Plain Text  : </small>;
  }
  
  renderAlert() {
    if (!this.props.error) return null;
    return (
        <UncontrolledAlert color="danger" role="alert">
            <h3>{this.props.error}</h3>
        </UncontrolledAlert>   
    );
  }
  
  handleScan(data){
    if(data){
      this.setState({
        result: data,
        delay:0,
        showCamera:!this.state.showCamera
      })
    }
  }
  
  handleError(error){
    console.log(error);
    this.setState({error});
  }  
  
  handleCameraToggle(){
    this.setState({showCamera:!this.state.showCamera});
  }
  
  renderQRReader() {
    if(this.state.result !== 'No result'){
      this.props.changeFieldValue(this.props.form, 'receiver', this.state.result.split('\n').join(' '))
    }
    if(this.state.showCamera) {
      return(
        [<QrReader
            delay={this.state.delay}
            onError={this.handleError}
            onScan={this.handleScan}
            style={{ width: '100%', maxWidth: '300px' }}
            />,
          <p>{this.state.result}</p>]);
    } else {
      return '';
    }
  }
  
  renderCipherSelect() {
    if(this.state.encrypted)
    return(<Row className="pb-3">           
            <InputGroup className="col-6 pr-auto">
                <Field component="select" name="cipherAlgo" className="form-control">{this.renderCipherOptions()}</Field>
            </InputGroup>
            <InputGroup className="col-6 pl-auto">
                <InputGroup prepend>
                  <Field component="input" type="text" name="passphrase" placeholder="Enter a Passphrase" className="form-control" aria-label="passphrase" aria-describedby="basic-addon1"/>                
                    <InputGroupText id="basic-addon1" title="Generate Random Salt" onClick={this.handleGeneratePassphrase.bind(this)}><i className="fa fa-refresh" ref="saltSpinner" /></InputGroupText>
                </InputGroup>
            </InputGroup>
            </Row>);
            
  }
  
  renderCipherOptions(){
    return crypto.getCiphers().map((cipher,i) => {
	if(i === 0)
	        return <option key={i} defaultValue={cipher} >{cipher}</option>;

	return <option key={i} value={cipher}>{cipher}</option>;
    });
  }
  
  handleEncryptionToggle(){
    this.setState({encrypted:!this.state.encrypted});
  }
  
  render() {
    return (
            [<Header key="header" />,
    <section key="body" className="bg-secondary">        
    	<Container fluid>
        <Container>
 				<Row>
 					<Col>
 						<h3>SEND A TRANSACTION
                <span className="float-right">
                  {this.renderEncryptedState()} &nbsp; <span className="switch switch-sm">
                    <input type="checkbox" className="switch" id="switch-sm" onChange={this.handleEncryptionToggle.bind(this)}/>
                    <label htmlFor="switch-sm"> </label>
                  </span>
                </span></h3>	
 					</Col>	
 				</Row>
 				<form onSubmit={this.handleSubmit(this.handleFormSubmit.bind(this))}>
 				<Row>
 					<Col>
 							<FormGroup className="form-check">
							  <label className="form-check-label w-100" htmlFor="exampleRadios1">
								Make sure this is correct or you will lose your coin.
							  </label>
                {this.renderAlert()}
              </FormGroup>
              {this.renderCipherSelect()}
						  <FormGroup>               
                <InputGroup append>
                  <InputGroup prepend>
                      <Field name="receiver" component="input" type="text"  className="form-control" placeholder="Add receiver public key"/>
                      <InputGroupText title="Scan QR" onClick={this.handleCameraToggle.bind(this)}><i className="fa fa-camera"/>{this.renderQRReader()}</InputGroupText>
                  </InputGroup>
                  <InputGroupText title="Look Up From Address Book" onClick={()=>{alert('Address Book View Popup')}}><i className="fa fa-address-book"/></InputGroupText>                
                </InputGroup>
						  </FormGroup>
						  <div className="form-row justify-content-center">
                <Col>
                  <Field name="amount" component="input" type="text" className="form-control" placeholder="amount"/>
                </Col>
						  </div>
						  <FormGroup>
                <Field name="message" component="textarea" type="text" className="form-control" rows="3" placeholder="Message" />
						  </FormGroup>
 					</Col>
 				</Row>
 				<Row>
					<Col size={12} sm={6} md={3} lg={3} >            
            {this.state.progress && !this.props.error ? <p >Processing...</p> : <Button color="primary" size="lg" type="submit" >Send</Button>}
            {this.props.hash ? <h3 >{this.props.hash}</h3> : ''}
  				</Col>
 				</Row>
			</form>	
      </Container>
      </Container>
 			</section>,
      <Footer />]
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ changeFieldValue, getLastTransaction, signTransaction, hashTransaction, sendTransaction}, dispatch);
}

const mapStateToProps = state => {
  return {
    users: state.users,
    progress:state.progress,
    auth: state.auth,
    error: state.transaction.error,
    hash: state.transaction.txHash,
    transaction: state.transaction
  };
};

SendTransaction = withRouter(connect(mapStateToProps, mapDispatchToProps)(SendTransaction));

export default reduxForm({
  form: 'send-transaction',
  fields: ['receiver', 'amount', 'stakeAmount', 'message', 'salt','passphrase','cipherAlgo']
})(SendTransaction);
