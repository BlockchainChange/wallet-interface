# WALLET INTERFACE #

Use this application to create your own wallet interface
and connect and send transactions on the blockchain. This repo is maintained by the founder.

### Help & The community ###

We encourge you to create pull requests and help us make the wallet interface into an awesome resource for others to interact with the blockchain.